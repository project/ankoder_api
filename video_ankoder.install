<?php


/**
 * @file
 * Provides installation functions for video_s3.module.
 *  @author Tamer Al Zoubi <tamerzg@gmail.com>
 */

/**
 * Implementation of hook_schema().
 */
function video_ankoder_schema() {
  $schema['video_ankoder'] = array(
    'description' => t('Store video s3 cdn and convert with ankoder webservice'),
    'fields' => array(
      'vid' => array(
        'description' => t('Auto Increment id'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'fid' => array(
        'description' => t('Original file id'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'nid' => array(
        'description' => t('Node id'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'jobid' => array(
        'description' => t('Job id'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'outputid' => array(
        'description' => t('Job Output id'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'bucket' => array(
        'type' => 'varchar',
        'length' => '255',
        'default' => '',
        'description' => t('The bucket the video is stored in.'),
      ),
      'filename' => array(
        'type' => 'varchar',
        'length' => '255',
        'default' => '',
        'description' => t('The filename of the video.'),
      ),
      'filepath' => array(
        'type' => 'varchar',
        'length' => '255',
        'default' => '',
        'description' => t('The filepath of the video.'),
      ),
      'filemime' => array(
        'type' => 'varchar',
        'length' => '255',
        'default' => '',
        'description' => t('The filemime of the video.'),
      ),
      'filesize' => array(
        'description' => t('Filesize of the video.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'dimensions' => array(
        'type' => 'varchar',
        'length' => '255',
        'default' => '',
        'description' => t('The dimensions of the video.'),
      ),
      'status' => array(
        'description' => t('Status of the cdn transfer'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'completed' => array(
        'description' => t('Time of successful completion to amazon.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
	  'thumbs' => array(
        'type' => 'text',
        'size' => 'medium',
        'default' => '',
        'description' => t('Thumbs.'),
      ),
    ),
    'indexes' => array(
      'status' => array('status'),
      'file' => array('fid'),
    ),
    'primary key' => array('vid'),
  );
  return $schema;
}

/**
 * Implementation of hook_install().
 */
function video_ankoder_install() {
  drupal_install_schema('video_ankoder');
  // set the module weight to low since we need this to load later time than in S3
  db_query("UPDATE {system} SET weight = 50 WHERE name = 'video_ankoder'");
}

/**
 * Implementation of hook_uninstall().
 */
function video_ankoder_uninstall() {
  drupal_uninstall_schema('video_ankoder');
  // TODO : Delete our variables.
}