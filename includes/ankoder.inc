<?php

/*
 * @file
 * Class file to handle amazon s3 transfers.
 * @author Tamer Al Zoubi <tamerzg@gmail.com>
 */
define('VIDEO_ZC_PENDING', 0);
define('VIDEO_ZC_WORKING', 1);
define('VIDEO_ZC_ACTIVE', 2);
define('VIDEO_ZC_FAILED', 3);

class video_ankoder_api {

  private $access_key;
  private $limit;
  private $bucket;

  public function __construct() {
    $this->access_key = variable_get('ankoder_access_key', '');
    $this->limit = variable_get('amazon_s3_limit', 5);
    $this->bucket = variable_get('amazon_s3_bucket', '');
  }

  /**
   * create transcoding job on ankoder.com
   */
  public function create($file) {
    global $base_url;
   
    // File details
    $filename = $file->filename;
    //file type
    $filetype = variable_get('video_ankoder_ext', 'flv');
    // Get varialbes
    $public = (variable_get('amazon_s3_private', FALSE)) ? 0 : 1;
    $width = variable_get('zc_width', '');
    $height = variable_get('zc_height', '');
    $stretch = variable_get('zc_stretch', '');
    $frame_rate = variable_get('zc_frame_rate', '');
    $keyframe_interval = variable_get('zc_key_frame_interval', '');
    $video_bitrate = variable_get('zc_vid_bit_rate', '');
	$video_codec = variable_get('zc_video_codec', 'flv');
    $audio_codec = variable_get('zc_audio_codec', 'aac');
    $audio_bitrate = variable_get('zc_audio_bitrate', '');
    $audio_channels = variable_get('zc_audio_channels', 2);
    $audio_sample_rate = variable_get('zc_audio_sample_rate', '');
    $thumb_no = variable_get('no_of_video_thumbs', 5);
    $thumb_base = $baseurl;
    $notify_url = variable_get('zc_notify_url', '');
    $start_clip = variable_get('zc_start_clip', '');
    $clip_length = variable_get('zc_clip_length', '');
    $bucket = $this->bucket;

    // Job details
    $input_name = $bucket . '/' . $filename;
    $zc_output = array();
   
    if (!empty($bucket))
      $zc_output['upload_url'] = 's3://' . $bucket . '/' . file_directory_path().'/videos';
	
	if (!empty($filetype))
      $zc_output['video_format'] =  $filetype;
	  
    $zc_output['video_codec'] = $video_codec;
	$zc_output['audio_codec'] = $audio_codec;
    if (!empty($width))
      $zc_output['width'] = $width;
    if (!empty($height))
      $zc_output['height'] = $height;
    if (!empty($frame_rate))
      $zc_output['video_fps'] = $frame_rate;
    if (!empty($video_bitrate))
      $zc_output['video_bitrate'] = $video_bitrate;
    if (!empty($audio_bitrate))
      $zc_output['audio_bitrate'] = $audio_bitrate;
    if (($audio_channels != 2))
      $zc_output['audio_channel'] = $audio_channels;
    if (!empty($audio_sample_rate))
      $zc_output['audio_sample_rate'] = $audio_sample_rate;
    if (!empty($start_clip))
      $zc_output['trim_begin'] = $start_clip;
    if (!empty($clip_length))
      $zc_output['duration'] = $clip_length;

    // thumbnails
    // Setup our thmbnail path.
    $video_thumb_path = variable_get('video_thumb_path', 'video_thumbs');
    $final_thumb_path = file_directory_path() . '/' . $video_thumb_path . '/' . $file->fid;

    if (!empty($bucket)){
     $zc_output['thumbnail_upload_url'] = 's3://' . $bucket . '/' . $final_thumb_path;
	 $zc_output['thumb_amount']=$thumb_no;
	 //TODO:
	 $zc_output['thumbnail_width']=160;
	 $zc_output['thumbnail_height']=120;
	}
  
    // Notifications
    $url = $base_url . '/postback/ankoder/jobs/'.$file->fid;
    $zc_output['postback_url'] = $url;
	$zc_output['url'] ='s3://' . $input_name . '';

   	$ankoder_access_key = variable_get('ankoder_access_key', '');
	$ankoder_date = date("r");
	$passkey = video_ankoder_get_passkey("POST","/download",$ankoder_date);
	$url = "http://api.ankoder.com/download";
	
	watchdog('ankoder', t('Sending job request to Ankoder with following data <pre>@debug</pre>').' <pre>@debug</pre>',array('@debug' =>print_r($zc_output,TRUE)));
	$curl = curl_init($url);
	$header = array("ankoder_access_key: $ankoder_access_key", "ankoder_passkey: $passkey", "ankoder_date: $ankoder_date");
	
	curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $zc_output);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$output =  curl_exec($curl);
	$id=video_ankoder_parse($output,"<id>");	
	watchdog('ankoder', t('Ankoder Reply<pre>@debug</pre>').' <pre>@debug</pre>',array('@debug' =>print_r($output,TRUE)));
	
	curl_close($curl);

    // Check if it worked
    if ($id) {
      return $id;
    } else {
      watchdog('ankoder', 'ankoder reports some errors.', WATCHDOG_ERROR);
      return false;
    }
  }

 
  /*
   * Updates the database after a successful transfer to amazon.
   */
  public function update($video) {
    $result = db_query("UPDATE {video_ankoder} SET filepath='%s', status=%d, completed=%d WHERE jobid=%d AND outputid = %d",
            $video->output->url, $video->output->state, time(), $video->job->id, $video->output->id);
    return $result;
  }

  /*
   * Verifies the existence of a file id, returns the row or false if none found.
   */
  public function load_job($jobid) {
    $sql = db_query("SELECT * FROM {video_ankoder} WHERE jobid=%d", $jobid);
    $row = db_fetch_object($sql);
    return $row;
  }
}