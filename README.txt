VIDEO_ANKODER.module
--------------------------------------------------------------------------------
This module implement the API for the Ankoder service to transcode uploaded videos. Module is integrated with Video module.

This module will need to have public IP to your development server, once you submit a job then Ankoder with postback some info to you once conversion is 
completed, then only autothumbnail and status update in the database will happen. If your developments machine doesn't have public IP go to 
video/plugins/video_ankoder/inludes/ankoder.inc file and replace line 149 with:
$url = 'http://url/of/drupal/site/postback/jobs';  //this url must be accessible from the public location.


--------------------------
Installation

1. Extract video_ankoder to your sites/all/modules directory. Make sure you have installed and enabled Video and Video_s3 modules.
2. Register account with Ankoder from http://app.ankoder.com/signup
3. You need to grant Write access to aws@ankoder.com so they can upload the processed file. The easiest way to do this is to use a S3 client, like S3Fox 
   or S3Hub. Please note that it is the S3 bucket and not the S3 folders, which are part of the key, that need Write permissions.
4. Go to Admin->Site Configurations->video->transcoders
5. Select Ankoder as your transcoder and type your credentials from step 2 to start using it.
6. Setup your transcoding settings.
7. Start uploading videos.
8. Run the cron and enjoy in your newly transcoded files!

--------------------------
What is Ankoder?
Web-based encoding software as a service, designed to quickly convert any video into web and mobile compatible formats.

--------------------------
Who is it for?
Web sites and applications that accept uploaded videos, and need to encode them to standard formats.

--------------------------
Why use it?
Building, maintaining, and scaling your own encoding solution is painful and expensive.

Features : http://www.ankoder.com/features/

---
The video_ankoder module has been originally developed by Tamer Zoubi under the sponsorship of the:
	PDM (http://premiumdrupalmodules.com) 
	Rorcraft Limited (http://www.rorcraft.com).