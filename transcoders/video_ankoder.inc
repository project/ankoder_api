<?php

/*
 * @file
 * Transcoder class file to handle ffmpeg_wrapper settings and conversions.
 * @TODO
 * - Cancel transcode job when delete.
 * - Add number of cronjobs pertime  set to 5 now.
 * - Replace load_completed_job with load_job
 * - Add a metadata extractor class to extract width and height
 * @author Tamer Al Zoubi <tamerzg@gmail.com>
 */

class video_ankoder implements transcoder_interface {

  private $name = 'Ankoder';
  private $value = 'video_ankoder';

  public function generate_thumbnails($video) {
    global $user;
    // Setup our thmbnail path.
    $video_thumb_path = variable_get('video_thumb_path', 'video_thumbs');
    $final_thumb_path = file_directory_path() . '/' . $video_thumb_path . '/' . $video['fid'];

    // Ensure the destination directory exists and is writable.
    $directories = explode('/', $final_thumb_path);
    // Get the file system directory.
    $file_system = file_directory_path();
    foreach ($directories as $directory) {
      $full_path = isset($full_path) ? $full_path . '/' . $directory : $directory;
      // Don't check directories outside the file system path.
      if (strpos($full_path, $file_system) === 0) {
        field_file_check_directory($full_path, FILE_CREATE_DIRECTORY);
      }
    }

    $files = array();
    // no thumbnails to generate
    $number_of_thumbs = variable_get('no_of_video_thumbs', 5);
    for ($i = 0; $i < $number_of_thumbs; $i++) {
      // @TODO Remove hard coded file types
      $filename = $video['fid'] . '_' . sprintf("%04d", $i) . '.png';
      $thumbfile = $final_thumb_path . '/' . $filename;
      //skip files already exists, this will save ffmpeg traffic
      if (!is_file($thumbfile)) {
        $default = drupal_get_path('module', 'video') . '/images/no-thumb.png';
        // Download generated thumbnails from S3.
        if (video_s3_get_object_info($thumbfile))
          $s3_get_object = video_s3_get_object($thumbfile, $thumbfile);
        else {
          $thumbfile = $final_thumb_path . '/no-thumb.png';
          file_copy($default, $thumbfile, FILE_EXISTS_REPLACE);
        }
//          $thumbfile = drupal_get_path('module', 'video') . '/images/no_thumb.gif';
        if (!file_exists($thumbfile)) {
          $error_param = array('%file' => $thumbfile, '%out' => $s3_get_object);
          $error_msg = t("Error downloading thumbnail for video: generated file %file does not exist.<br />S3 Output:<br />%out", $error_param);
          // Log the error message.
          watchdog('ankoder', $error_msg, array(), WATCHDOG_ERROR);
          continue;
        }
      }
      // Begin building the file object.
      // @TODO : use file_munge_filename()
      $file = new stdClass();
      $file->uid = $user->uid;
      $file->status = FILE_STATUS_TEMPORARY;
      $file->filename = trim($filename);
      $file->filepath = $thumbfile;
      $file->filemime = file_get_mimetype($filename);
      $file->filesize = filesize($thumbfile);
      $file->timestamp = time();
      $files[] = $file;
    }
    return $files;
  }

  public function video_converted_extension() {
    return variable_get('video_ankoder_ext', 'flv');
  }

  public function convert_video($video) {
    // get the active jobs and check for the status
    if ($video->video_status == VIDEO_RENDERING_ACTIVE) {

      return;
    }
    // We need to check if this file id exists in our S3 table to avoid file not found error.
    $s3_video = db_query("SELECT * FROM {video_s3} WHERE fid=%d", $video->fid);
    if ($s3_file = db_fetch_object($s3_video)) {
      // This is a s3 file, lets verify it has been pushed and if so lets push to ankoder queue.
      if ($s3_file->status == VIDEO_S3_COMPLETE) {
        $video_s3 = $s3_file;
      }
    } else {
      watchdog('ankoder', t('You must activate the Video S3 module to work with ankoder, file is not found in S3 tables', array()), array(), WATCHDOG_ERROR);
//      return FALSE;
    }

    // If we have a video lets go ahead and send it.
    if (is_object($video_s3)) {
      // This will update our current video status to active.
//      $this->change_status($video->vid, VIDEO_RENDERING_ACTIVE);
      // Get Output file info
      $filetype = variable_get('video_ankoder_ext', 'flv');
      // bucket name
      $bucket = variable_get('amazon_s3_bucket', '');
      $ssl = variable_get('amazon_s3_ssl', FALSE);

      $filepath = $video_s3->filepath;
      $filename = $video_s3->filename;
      module_load_include('inc', 'video_ankoder', '/includes/ankoder');
	  $zc = new video_ankoder_api;
      if ($encoding_job = $zc->create($video_s3)) {
//        print_r($encoding_jobxx);
//        exit;
        // Update our table.
        $video->bucket = $bucket;
        $video->filename = $filename . '.' . $filetype;
        $video->filemime = file_get_mimetype($filename . '.' . $filetype);
        $video->vid = $video->vid;
        $video->jobid = $encoding_job;
        // @TODO : add foreach for each outout and put insert save multiple files
        $prefix = $ssl ? 'https://' : 'http://';
        $video->filepath = $prefix . $video->bucket . '.s3.amazonaws.com/' . $filename . '.' . $filetype;

        if ($this->update($video)) {
          watchdog('ankoder', t('Successfully created trancoding job on file: !file into the bucket %bucket on ankoder.', array('!file' => $filepath, '%bucket' => $bucket)), array(), WATCHDOG_INFO);
        }
      } else {
        watchdog('ankoder', 'Failed to queue our file to ankoder. <pre>@debug</pre>', array('@debug' =>print_r($video,TRUE)), WATCHDOG_ERROR);
        $this->change_status($video->vid, VIDEO_RENDERING_FAILED);
        return FALSE;
      }
    } else {
      watchdog('ankoder', 'We did not find the file id: ' . $video->fid . ' or it is still queued for ffmpeg processing or S3 push.', array(), WATCHDOG_DEBUG);
    }
  }

  /**
   * Interface Implementations
   * @see sites/all/modules/video/includes/transcoder_interface#get_name()
   */
  public function get_name() {
    return $this->name;
  }

  /**
   * Interface Implementations
   * @see sites/all/modules/video/includes/transcoder_interface#get_value()
   */
  public function get_value() {
    return $this->value;
  }

  /**
   * Interface Implementations
   * @see sites/all/modules/video/includes/transcoder_interface#get_help()
   */
  public function get_help() {
    $help = l(t('ankoder'), 'http://ankoder.com/');
    return $help;
  }

  /**
   * Interface Implementations
   * @see sites/all/modules/video/includes/transcoder_interface#admin_settings()
   */
  public function admin_settings() {
    global $user;
    // check amazon s3 module is exists or not
    if (!module_exists('video_s3'))
      drupal_set_message(t('You must enable Video Amazon S3 Module to enable this module.'), 'error');

    $form = array();
    $form['video_ankoder_start'] = array(
      '#type' => 'markup',
      '#value' => '<div id="video_ankoder">',
    );

      $form['ankoder_info'] = array(
        '#type' => 'fieldset',
        '#title' => t('Ankoder API'),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
      );
      $form['ankoder_info']['ankoder_access_key'] = array(
        '#type' => 'textfield',
        '#title' => t('Ankoder Access Key'),
        '#default_value' => variable_get('ankoder_access_key', ''),
        '#size' => 50,
        '#description' => t('Ankoder Access Key.')
      );
	  
	  $form['ankoder_info']['ankoder_private_key'] = array(
        '#type' => 'textfield',
        '#title' => t('Ankoder Private Key'),
        '#default_value' => variable_get('ankoder_private_key', ''),
        '#size' => 50,
        '#description' => t('Ankoder Private Key.')
      );

      $form['ankoder'] = array(
        '#type' => 'fieldset',
        '#title' => t("Ankoder Settings"),
        '#collapsed' => false,
      );

      // output video type
	  $video_format = array(
        '3g2'=>'3g2',
		'3gp'=>'3gp',
		'asf'=>'asf',
		'avi'=>'avi',
		'dvd'=>'dvd',
		'flv'=>'flv',
		'mjpeg'=>'mjpeg',
		'mov'=> 'mov',
		'mp3'=> 'mp3',
		'mp4'=> 'mp4',
		'mpeg'=>'mpeg',
		'mpeg2video'=>'mpeg2video',
		'mpegts'=>'mpegts',
		'ogg'=>'ogg',
		'psp'=>'psp',
		'rm'=>'rm',
		'svcd'=>'svcd',
		'vcd'=>'vcd',
		'vob'=>'vob',
		'matroska'=>'matroska',
		'wmv'=>'wmv',
      );
      $form['ankoder']['video_ankoder_ext'] = array(
        '#type' => 'select',
        '#title' => t('Video Format'),
		'#required' => TRUE,
		'#options' => $video_format,
        '#default_value' => variable_get('video_ankoder_ext', 'flv'),
        '#description' => t('The video extensions.')
      );
	  
	  $video_codec = array(
		'flv'=>'flv',
		'mjpeg'=>'mjpeg',
		'h263'=> 'h263',
		'h264'=> 'h264',
		'huffyuv'=> 'huffyuv',
		'mp4'=>'mp4',
		'mpeg4'=>'mpeg4',
		'xvid'=>'xvid',
		'theora'=>'theora',
		'msmpeg4v2 '=>'msmpeg4v2 ',
		'wmv1'=>'wmv1',
		'wmv2'=>'wmv2',		
      );
      $form['ankoder']['zc_video_codec'] = array(
        '#type' => 'select',
        '#title' => t('Video Codec'),
		'#required' => TRUE,
        '#options' => $video_codec,
        '#default_value' => variable_get('zc_video_codec', 'flv'),
        '#description' => t('The video codec used in the video file can affect the ability to play the video on certain devices. The default codec used is FLV and should only be changed if the playback device you are targeting requires something different.'),
      );
	  
	   $audio_codec = array(
        'aac' => 'AAC - Default',
        'mp3' => 'MP3',
		'ac3'=>'Ac3',
		'amr'=>'Amr',
		'flac'=>'Flac',
		'vorbis'=>'Vorbis',
		'wmav2'=>'Wmav2',
		'mpeg4aac'=>'Mpeg4aac',
		'mp2'=>'Mp2',
		
      );
      $form['ankoder']['zc_audio_codec'] = array(
        '#type' => 'select',
        '#title' => t('Audio Codec'),
		'#required' => TRUE,
        '#options' => $audio_codec,
        '#default_value' => variable_get('zc_audio_codec', 'aac'),
        '#description' => t('The audio codec used in the video file can affect the ability to play the video on certain devices. The default codec used is AAC and should only be changed if the playback device you are targeting requires something different.'),
      );

      // Basic Video Settings
      $form['ankoder']['basic_video'] = array(
        '#type' => 'fieldset',
        '#title' => t("Basic Video Settings"),
        '#collapsed' => true,
        '#collapsible' => true,
        '#description' => t('')
      );

      $form['ankoder']['basic_video']['zc_width'] = array(
        '#type' => 'textfield',
        '#title' => t('Video width'),
        '#default_value' => variable_get('zc_width', ''),
        '#description' => t('Width of the converted video, of the format 600x400. This is the maximum width of the output video specified as a positive integer. In order for video compression to work properly the width should be divisible by 4 (or even better 16).'),
        '#size' => 12,
      );
      $form['ankoder']['basic_video']['zc_height'] = array(
        '#type' => 'textfield',
        '#title' => t('Video height'),
        '#default_value' => variable_get('zc_height', ''),
        '#description' => t('Width of the converted video, of the format 600x400. This is the maximum height of the output video specified as a positive integer. In order for video compression to work properly the height should be divisible by 4 (or even better 16).'),
        '#size' => 12,
      );
      
      

      // Advaced Video Settings
      $form['ankoder']['adv_video'] = array(
        '#type' => 'fieldset',
        '#title' => t("Advanced Video Settings"),
        '#collapsed' => true,
        '#collapsible' => true,
        '#description' => t('')
      );

      
      $form['ankoder']['adv_video']['zc_stretch'] = array(
        '#type' => 'checkbox',
        '#title' => t('Stretch?'),
        '#description' => t("If true, the aspect ratio of the original file may be distorted to fit the aspect ratio of the supplied width and height. By default, aspect ratio will always be preserved."),
        '#default_value' => variable_get('zc_stretch', ''),
      );
      $form['ankoder']['adv_video']['zc_frame_rate'] = array(
        '#type' => 'textfield',
        '#title' => t('Frame Rate '),
        '#default_value' => variable_get('zc_frame_rate', ''),
        '#description' => t('The output frame rate to use specified as a decimal number (e.g. 15 or 24.98). Unless you need to target a specific frame rate, you might be better off using Maximum Frame Rate setting. By default, the original frame rate will be preserved.'),
        '#size' => 12,
      );
      
      
      $form['ankoder']['adv_video']['zc_vid_bit_rate'] = array(
        '#type' => 'textfield',
        '#title' => t('Video Bitrate'),
        '#default_value' => variable_get('zc_vid_bit_rate', ''),
        '#description' => t('The target video bitrate specified as kilobits per second (Kbps, e.g. 300 or 500).'),
        '#size' => 12,
      );
 
      // Advanced Audio Settings
      $form['ankoder']['adv_audio'] = array(
        '#type' => 'fieldset',
        '#title' => t("Advanced Audio Settings"),
        '#collapsed' => true,
        '#collapsible' => true,
        '#description' => t('')
      );
     
      $form['ankoder']['adv_audio']['zc_audio_bitrate'] = array(
        '#type' => 'textfield',
        '#title' => t('Audio Bitrate'),
        '#default_value' => variable_get('zc_audio_bitrate', ''),
        '#description' => t('The overall audio bitrate specified as kilobits per second (Kbps, e.g. 96 or 160). This value can\'t exceed 160 Kbps per channel. 96-160 is usually a good range for stereo output.'),
        '#size' => 12,
      );
      $audio_channel = array(
        1 => 'Mono',
        2 => 'Stereo - Default'
      );
      $form['ankoder']['adv_audio']['zc_audio_channels'] = array(
        '#type' => 'select',
        '#title' => t('Audio Channels'),
        '#options' => $audio_channel,
        '#default_value' => variable_get('zc_audio_channels', 2),
        '#description' => t('By default we will choose the lesser of the number of audio channels in the input file or 2 (stereo).'),
      );
      $form['ankoder']['adv_audio']['zc_audio_sample_rate'] = array(
        '#type' => 'textfield',
        '#title' => t('Audio Sample Rate'),
        '#default_value' => variable_get('zc_audio_sample_rate', ''),
        '#description' => t('The sample rate of the audio file specified in hertz (e.g. 44100 or 22050). A sample rate of 44100 is the best option for web playback. 22050 and 48000 are also valid options. Warning: the wrong setting may cause encoding problems. By default, 44100 will be used.'),
        '#size' => 12,
      );
     
      // Other Settings
      $form['ankoder']['other'] = array(
        '#type' => 'fieldset',
        '#title' => t("Other Settings"),
        '#collapsed' => true,
        '#collapsible' => true,
        '#description' => t('')
      );
      $form['ankoder']['other']['zc_start_clip'] = array(
        '#type' => 'textfield',
        '#title' => t('Start Clip'),
        '#default_value' => variable_get('zc_start_clip', ''),
        '#description' => t('Creates a subclip from the input file, starting at either a timecode or a number of seconds. Format: 88.6 for 1 minute and 23.6 seconds.'),
        '#size' => 12,
      );
      $form['ankoder']['other']['zc_clip_length'] = array(
        '#type' => 'textfield',
        '#title' => t('Clip Length'),
        '#default_value' => variable_get('zc_clip_length', ''),
        '#description' => t('Creates a subclip from the input file of the specified length using either a timecode or a number of seconds. 45.3 for 45.3 seconds.'),
        '#size' => 12,
      );
  //  }
    $form['video_ankoder_end'] = array(
      '#type' => 'markup',
      '#value' => '</div>',
    );
    return $form;
  }

  /**
   * Interface Implementations
   * @see sites/all/modules/video/includes/transcoder_interface#admin_settings_validate()
   */
  public function admin_settings_validate($form, &$form_state) {
    $ankoder_api = variable_get('ankoder_access_key', null);
    if (isset($ankoder_api) && !empty($ankoder_api) || $form_state['values']['vid_convertor'] != 'video_ankoder')
      return;

    $errors = false;
  }

  /**
   * Return the dimensions of a video
   */
  public function get_dimensions($video) {
    // @TODO get object properties
    return;
  }

  public function create_job($video) {
    return db_query("INSERT INTO {video_ankoder} (fid, status, dimensions) VALUES (%d, %d, '%s')", $video['fid'], VIDEO_RENDERING_PENDING, $video['dimensions']);
  }

  public function update_job($video) {
    if (!$this->load_job($video['fid']))
      return;
    //lets update our table to include the nid
    db_query("UPDATE {video_ankoder} SET nid=%d WHERE fid=%d", $video['nid'], $video['fid']);
  }

  public function delete_job($video) {
    if (!$this->load_job($video->fid))
      return;
    //lets get all our videos and unlink them
    $sql = db_query("SELECT filepath FROM {video_ankoder} WHERE fid=%d", $video->fid);
    //we loop here as future development will include multiple video types (HTML 5)
    while ($row = db_fetch_object($sql)) {
      // @TODO : cancel the job to transcode
    }
    //now delete our rows.
    db_query('DELETE FROM {video_ankoder} WHERE fid = %d', $video->fid);
  }

  public function load_job($fid) {
    $job = null;
    $result = db_query('SELECT f.*, vf.vid, vf.nid, vf.status as video_status FROM {video_ankoder} vf LEFT JOIN {files} f ON vf.fid = f.fid WHERE f.fid=vf.fid AND f.fid = %d', $fid);
    $job = db_fetch_object($result);
    if (!empty($job))
      return $job;
    else
      return FALSE;
  }

  public function load_job_queue() {
    // load jobs with status as pending and active both
    $total_videos = variable_get('video_instances', 5);
    $videos = array();
    $result = db_query_range('SELECT f.*, vf.vid, vf.nid, vf.status as video_status FROM {video_ankoder} vf LEFT JOIN {files} f ON vf.fid = f.fid WHERE vf.status = %d AND f.status = %d ORDER BY f.timestamp',
            VIDEO_RENDERING_PENDING, FILE_STATUS_PERMANENT, 0, $total_videos);

    while ($row = db_fetch_object($result)) {
      $videos[] = $row;
    }
    return $videos;
  }

  /**
   * @todo : replace with the load job method
   * @param <type> $video
   * @return <type>
   */
  public function load_completed_job($video) {
    module_load_include('inc', 'video_s3', '/includes/amazon_s3');
    $video = db_fetch_object(db_query('SELECT * FROM {video_ankoder} WHERE fid = %d', $video->fid));

    $s3 = new video_amazon_s3;
    if (variable_get('amazon_s3_private', FALSE))
      $video->filepath = video_s3_get_authenticated_url($video->filename);
    else
      $video->filepath = $video->filepath;

    return $video;
  }

  /**
   * Change the status of the file.
   *
   * @param (int) $vid
   * @param (int) $status
   */
  public function change_status($vid, $status) {
    $result = db_query('UPDATE {video_ankoder} SET status = %d WHERE vid = %d ', $status, $vid);
  }

  /*
   * Updates the database after a successful transfer to amazon.
   */
  private function update($video) {
    $result = db_query("UPDATE {video_ankoder} SET jobid = %d, outputid = %d, bucket='%s', filename='%s', filepath='%s', filemime='%s', filesize='%s', completed=%d WHERE vid=%d",
            $video->jobid, $video->outputid, $video->bucket, $video->filename, $video->filepath, $video->filemime, $video->filesize, time(), $video->vid);
    return $result;
  }

}